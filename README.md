Symfony3 Blog creation docs
===========================

These docs are based on the original [Symblog - Creating a blog in Symfony2](http://tutorial.symblog.co.uk/index.html) tutorial.
The docs were also uploaded to [GitHub](https://github.com/dsyph3r/symblog-docs), which I forked.


What's planned?
===============

I am going to modify these docs. 

I am changing these to fit and work with *Symfony 3.x* and I am going to reduce 
these down to less talk - keeping only the reasong why something was done that way.

I also plan to expand on these and add anything that I add to my build of the blog into these instructions.

Why? ... well the docs haven't been updated in a while and some of the coding 
methods don't work or are getting near the state of not working. I did a build on Symfony 2.8 and
needed some minor changes, but had a few deprecated stuff.

I feel changing to Symfony3 _NOW_ is the best time to get this fixed and working nicely before Symfony3 LTS is out.

Symfony 3.4 is the next LTS and should but out in Nov 2017, so I have time to get things working the way I want, debug everything and whatever...
